# XFCE

XFCE desktop environment build scripts for Slackware Linux.

# 

This is a *mostly* complete set of build scripts to build a complete XFCE environment from scratch on a Slackware base with no desktop environment installed. (Of course you can build it with other DE/WMs installed, make sure to check for duplicate packages (deps) first!)

This repo is in it's eary stages so tweaks, optional dependencies (within the XFCE tree) and build scripts could need to be adjusted.

Help is always appreciated! Additions are always considered, considering that they come only from the XFCE tree.

/optional contains gst-libav, gst-plugins-bad and gst-plugins-ugly that are optional dependencies for parole.

## Sunset

xfce4-datetime-plugin  
  -As of 0.8.3 xfce4-datetime-plugin is EOL. Switch to the clock plugin.


# 

Slackware® is a registered trademark of [Patrick Volkerding](http://www.slackware.com/)  
Linux® is a registered trademark of [Linus Torvalds](http://www.linuxmark.org/)
